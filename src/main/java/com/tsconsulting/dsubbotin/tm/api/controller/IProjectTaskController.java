package com.tsconsulting.dsubbotin.tm.api.controller;

public interface IProjectTaskController {

    void bindTaskToProject();

    void unbindTaskFromProject();

    void findAllTasksByProjectId();

    void removeProjectById();

    void removeProjectByIndex();

    void removeProjectByName();

}
