package com.tsconsulting.dsubbotin.tm.api.controller;

public interface ITaskController {

    void createTask();

    void showTasks();

    void clearTasks();

    void showById();

    void showByIndex();

    void showByName();

    void removeById();

    void removeByIndex();

    void removeByName();

    void updateById();

    void updateByIndex();

    void startById();

    void startByIndex();

    void startByName();

    void finishById();

    void finishByIndex();

    void finishByName();

    void updateStatusById();

    void updateStatusByIndex();

    void updateStatusByName();

}
