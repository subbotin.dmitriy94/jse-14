package com.tsconsulting.dsubbotin.tm.api.repository;

import com.tsconsulting.dsubbotin.tm.model.Command;

public interface ICommandRepository {

    Command[] getTerminalCommands();

}
